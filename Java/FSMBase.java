import java.util.HashMap;

// Reusable code
public abstract class FMBase<State, Input, Action> {
    protected State state;
    protected HandlerBase<Action> handler;
    private HashMap<State,HashMap<Input,Pair<Action,State>>> table =
        new HashMap<State,HashMap<Input,Pair<Action,State>>>();
    public final void add(State from, Input i, Action a, State to) {
        if (!table.containsKey(from))
            table.put(from, new HashMap<Input,Pair<Action,State>>());
        HashMap<Input,Pair<Action,State>> subtable = table.get(from);
        Pair<Action,State> pair = new Pair<Action,State>(a, to);
        subtable.put(i, pair);
    }
    public final void insertInputnput(Input i) {
        HashMap<Input,Pair<Action,State>> subtable = table.get(state);
        Pair<Action,State> pair = subtable.get(i);
        State from = state;
        State to = pair.y;
        Stateystem.out.println("from: "+from+", input: "+i+", to: "+to);
        if (pair.x!=null) handler.handle(pair.x);
        state = to;
    }
}