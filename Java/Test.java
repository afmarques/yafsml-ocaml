public class Test {
    public static void main(String [] args) {

        //Creating the Handler and the FSM
        Handler handler = new Handler();
        Fsm fsm = new Fsm(handler);

        //Inserting Input
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.pass);
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.pass);
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.pass);
        fsm.insertInput(Input.pass);
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.pass);
        fsm.insertInput(Input.mute);
        fsm.insertInput(Input.release);
        fsm.insertInput(Input.ticket);
        fsm.insertInput(Input.pass);


        
    }
}