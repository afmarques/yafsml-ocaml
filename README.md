
# Yet Another FSML in OCaml

	1. How to run
	2. Example of a FSM to insert on main
	3. Example of input to the FSM
	4. Objective
	5. Metodology
	6. Workplan
	7. Implementation

## HOW TO RUN

The code is already compiled, so all you need to do, is download all the folders.

	1. Open your terminal
	2. Go to _build folder with "cd _build"
	3. Write "./main.byte" and follow the instructions.

The Java code will be in a folder named Java, to go there after the program runs, just do "cd ../Java", the FSM.dot file is in the root, so if you are on the Java folder or on the _build folder just do "cd .." .

## EXAMPLE OF A FSM TO INSERT ON MAIN

initial state locked {
ticket/collect -> unlocked;
pass/alarm -> exception;
}
state unlocked {
ticket/eject;
pass -> locked;
}
state exception {
ticket/eject;
pass;
mute;
release -> locked;
}.

## EXAMPLE OF INPUT TO THE FSM

[ticket, pass, ticket, pass, ticket, ticket, pass, pass, ticket, pass, mute, release, ticket, pass].

## OBJECTIVE


Do a Final State Machine Language (FSML), using OCaml,
for Software Language Engineering (SLE), i.e. accademic purposes.

## METODOLOGY


Using a SCRUM metodology and sprints of 1 week.


## WORKPLAN

	 sprints 	Date (the day it starts - day it ends)
	 	1				29.Nov - 5.Dec
	 	2				6.Nov - 12.Dec
	 	3				13.Dec - 19.Dec
	 	4				20.Dec - 26.Dec
	 	5				27.Dec - 2.Jan
	 	6				3.Jan - 9.Jan


(the workplan may differ since the scrum metodology requires a iterative check of:
	 what is being done;
	 what is done;
	 what still needs to be done;
	 bug fixes and evaluation of what was implemented.)


														When will be finished
	 		Things to do 							  (aproximately - sprints)	

     parser for the concrete textual syntax						   1
	 additional abstract syntax;								   1
	 ## bug fixes and alterations ##							   3
	 check all the constraints;									   3
	 implement the reference (simulation) semantics;			   4
	 ## bug fixes and alterations ##							   4
	 %DOCUMENTATION% short progress report 						   -
	 implement the code generator;         	                       6
	 ## bug fixes and alterations ##							   6
	 represent FSMs visually.             	                       6
	 ## bug fixes and alterations ##							   6
	 %DOCUMENTATION% final presentation	 						   6



## IMPLEMENTATION

+ Sprint 6 (3.Jan - 9.Jan)

	- What is implemented:
		* parser for the concrete textual syntax;  # FROM SPRINT 1
		* additional abstract syntax; # FROM SPRINT 1
		* bugs fixes on Parser and Syntax; # FROM SPRINT 3
		* check all the constraints; # FROM SPRINT 3
		* implement the reference (simulation) semantics; # FROM SPRINT 4 (but edit in 5)

	- what is being done:
		* resolving bugs from the code generator;
		* represent FSMs visually;
		* bugs from the FSM visualizer.		

-------------------------------------------------

+ Sprint 5 (27.Dec - 2.Jan)

	- What is implemented:
		* parser for the concrete textual syntax;  # FROM SPRINT 1
		* additional abstract syntax; # FROM SPRINT 1
		* bugs fixes on Parser and Syntax; # FROM SPRINT 3
		* check all the constraints; # FROM SPRINT 3

	- what is being done:
		* resolving bugs from the reference (simulation) semantics; 
		* implement the code generator;


	- What still needs to be done:
		* represent FSMs visually.

-------------------------------------------------

+ Sprint 4 (20.Dec - 26.Dec)

	- What is implemented:
		* parser for the concrete textual syntax;  # FROM SPRINT 1
		* additional abstract syntax; # FROM SPRINT 1
		* bugs fixes on Parser and Syntax; # FROM SPRINT 3
		* check all the constraints; # FROM SPRINT 3

	- what is being done:
		* implement the reference (simulation) semantics;


	- What still needs to be done:
		* implement the code generator;
		* represent FSMs visually.

-------------------------------------------------

+ Sprint 3 (13.Dec - 19.Dec)

	- What is implemented:
		* parser for the concrete textual syntax;  # FROM SPRINT 1
		* additional abstract syntax; # FROM SPRINT 1
		* bugs fixes on Parser and Syntax; # FROM SPRINT 2
		* check all the constraints;

	- what is being done:
		* bugs fixes on the constrains;


	- What still needs to be done:
		* implement the code generator;
		* represent FSMs visually.

-------------------------------------------------		

+ Sprint 2 (6.Nov - 12.Dec)

	- What is implemented:
		* parser for the concrete textual syntax;  # FROM SPRINT 1
		* additional abstract syntax; # FROM SPRINT 1

	- what is being done:
		* more bugs fixes on Parser and Syntax (had to to a few changes);
		* bug fixes on the main.ml
		* check all the constraints;


	- What still needs to be done:
		* implement the reference (simulation) semantics;
		* implement the code generator;
		* represent FSMs visually.

-------------------------------------------------

+ Sprint 1 (29.Nov - 5.Dec)

	- What is implemented:
		* parser for the concrete textual syntax;
		* additional abstract syntax;

	- what is being done:
		* check all the constraints;
		* bug fixes on Parser and syntax;

	- What still needs to be done:
		* implement the reference (simulation) semantics;
		* implement the code generator;
		* represent FSMs visually.