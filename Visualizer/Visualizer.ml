open Syntax
open Semantics
open Printf
open Generator

let rec getInputsFrom fsm temp name=
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> getAcStiF x temp name
									| x::xs -> (getAcStiF x temp name)@ unfold xs) in unfold m
and getAcStiF st temp name=
	match st with
	| InitialState (l,r) -> if (l=name) then getTrsiF r temp else []
  | State (l,r) -> if (l=name) then getTrsiF r temp else []
and getTrsiF at temp=
	match at with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> get_triF x temp 
									| x::xs -> (get_triF x temp)@unfold xs ) in (unfold b)
and get_triF actl temp = 
	match actl with
	| ActionLine (l,_,_) -> if List.mem l temp then [] else l::temp ; [l]
;;

let getEdges fsm hashTable = 
	let statesList = getStates fsm in
  		let rec unfold l =
  			(match l with
  			| [] -> ""
  			| x::[] -> let v = Hashtbl.find hashTable x in
										 let inputList = getInputsFrom fsm [] x in
  										let rec unfold2 ls =
												(match ls with
												| [] -> ""
												| a::[] -> if Hashtbl.mem v a then
																		(let nextS = List.nth (Hashtbl.find v a) 1 in
																			let action = List.hd (Hashtbl.find v a) in
																				if (action <> " ") then 
																				 (x^" -> "^nextS^"[label=\""^a^"/"^action^"\"];\n")
																				else x^" -> "^nextS^"[label=\""^a^"\"];\n") else ""
												| a::ax -> if Hashtbl.mem v a then 
																		(let nextS = List.nth (Hashtbl.find v a) 1 in
																			let action = List.hd (Hashtbl.find v a) in
																				if (action <> " ") then 
																				 (x^" -> "^nextS^"[label=\""^a^"/"^action^"\"];\n"^unfold2 ax)
																				else x^" -> "^nextS^"[label=\""^a^"\"];\n"^unfold2 ax) else unfold2 ax ) in unfold2 inputList
				| x::xs  -> let v = Hashtbl.find hashTable x in
											let inputList = getInputsFrom fsm [] x in
    										let rec unfold2 ls =
  												(match ls with
  												| [] -> ""
  												| a::[] -> if Hashtbl.mem v a then 
  																		(let nextS = List.nth (Hashtbl.find v a) 1 in
  																			let action = List.hd (Hashtbl.find v a) in
  																				 if (action <> " ") then 
																				 		(x^" -> "^nextS^"[label=\""^a^"/"^action^"\"];\n")
																					else x^" -> "^nextS^"[label=\""^a^"\"];\n") else ""
  												| a::ax -> if Hashtbl.mem v a then 
  																		(let nextS = List.nth (Hashtbl.find v a) 1 in
																				let action = List.hd (Hashtbl.find v a) in
    																			if (action <> " ") then 
  																				 	(x^" -> "^nextS^"[label=\""^a^"/"^action^"\"];\n"^unfold2 ax)
  																				else x^" -> "^nextS^"[label=\""^a^"\"];\n"^unfold2 ax) else unfold2 ax ) in (unfold2 inputList)^(unfold xs) ) in unfold statesList	
;;

let rec getVertices fsm =
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> ""
									| x::[] -> getV x^";\n"
									| x::xs -> getV x^";\n"^unfold xs) in unfold m
and getV st =
	match st with
	| InitialState (l,_) -> l^"[label=\""^l^"\",shape=ellipse,style=filled]"
  | State (l,_) -> l^"[label=\""^l^"\",shape=ellipse]"
;;

let show fsm =
	let hash = createfsmDT fsm in
    let file = "../fsm.dot" in 
    	let oc = open_out file in
    			let creatingDigraph = "digraph G {\n" in
    				let creatingVertices = getVertices fsm in
  						let createEdges = getEdges fsm hash in 
								let closeEverything = "}" in
									Printf.fprintf oc "%s\n" creatingDigraph;
  								Printf.fprintf oc "%s\n" creatingVertices;
									Printf.fprintf oc "%s\n" createEdges;
									Printf.fprintf oc "%s\n" closeEverything;
									close_out oc;	
		
	
	
;;