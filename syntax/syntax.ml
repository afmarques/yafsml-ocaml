type fsm =
	| FSM of st list
and st =
	| InitialState of string * alt
	| State of string * alt
and alt =
	| AllTrans of act_line list
and act_line =
	| ActionLine of string * string list * trans
and trans = 
	| Trans of string list
;; 

exception Exec_err of string ;;

let rec abstr a =
	match a with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> ""
									| x::[] -> "\n("^abstr_st x ^"\n"
									| x::xs -> "\n("^abstr_st x ^",\n"^ unfold xs) in "["^(unfold l)^"]."
and abstr_st b =
	match b with
	| InitialState (l,r) -> "true,"^l^",[\n"^(abstr_trans r l)^"])"
	| State (l,r) -> "false,"^l^",[\n"^(abstr_trans r l)^"])"
and abstr_trans c name=
	match c with
	| AllTrans n -> let rec unfold l =
									(match l with
									| [] -> ""
									| x::[] -> abstr_al x name
									| x::xs -> (abstr_al x name) ^",\n"^(unfold xs)) in (unfold n)
and abstr_al d name=
	match d with
	| ActionLine (l,m,r) -> let rec unfold l =
													(match l with 
													| [] ->  ",[]"
													| x::[] -> ",["^x^"]"
													| x::xs -> raise (Exec_err "Too many actions")) in ("("^l^(unfold m) ^(abstr_tr r name))
and abstr_tr e name=
	match e with
	| Trans l -> let rec unfold l =
											(match l with
											| [] ->  ","^name^")"
											| x::[] -> ","^x^")"
											| x::xs -> raise (Exec_err "Too many transitions")) in (unfold l)	
;;