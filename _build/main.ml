open Parser
open Syntax
open Constrains
open Semantics
open Generator
open Visualizer

let visual fsm =
	print_string "Creating .dot file from FSM visualization..\n" ;
	Visualizer.show fsm ;
	print_string "fsm.dot file created" ;
;;

let generate fsm=
	print_newline () ;
	print_string "Generating Java code...\n" ;
	Generator.create fsm;
	print_string "Java code generated!\n" ;
;;

let rec prompt2 fsm=
	print_string "Please write the input for the FSM:\n> " ;
	flush stdout;
	let x = input_line stdin in
		let v = Semantics.read_input fsm x in
			print_string "Output from the FSM:\n" ;
			if (v = "[\nERROR1].") then ( print_endline "> An Infeasible Symbol was found!\n" ; prompt2 fsm)
			else if (v = "[\nERROR2].") then (print_endline "> An Invalid Symbol was found!\n" ; prompt2 fsm)
			else (print_endline v ; generate fsm ; visual fsm)
;;

let rec prompt lexbuf =
	print_string "Please write your fsm:\n> " ;
	flush stdout;
  try
  	let s = Parser.main Lexer.token lexbuf in
			let ab = abstr s in
				let c = check_contrains s in
					print_string ab ; print_newline () ; print_string c ; print_newline () ; 
					if (c = "0 contrains broken:\n" ) then prompt2 s
					else prompt lexbuf
  with
   	Parsing.Parse_error -> print_string "Parsing error\n" ; prompt lexbuf
	| End_of_file -> ()

let main () =
	let lexbuf = Lexing.from_channel (stdin) in prompt lexbuf
;;

main();;
