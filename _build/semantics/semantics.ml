open Syntax

exception Exec_err of string ;;


let rec getInputs fsm temp=
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> getAcSti x temp
									| x::xs -> (getAcSti x temp)@ unfold xs) in unfold m
and getAcSti st temp=
	match st with
	| InitialState (_,r) -> getTrsi r temp
  | State (_,r) -> getTrsi r temp
and getTrsi at temp=
	match at with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> get_tri x temp 
									| x::xs -> (get_tri x temp)@unfold xs ) in (unfold b)
and get_tri actl temp = 
	match actl with
	| ActionLine (l,_,_) -> if List.mem l temp then [] else l::temp ; [l]
;;

let rec count_trans state name =							
	match state with
	| InitialState (l,r) -> if (l=name) then () ; count_t r 
	| State (l,r) -> if (l=name) then () ; count_t r
and count_t r=
	match r with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> 0 
									| x::[] -> 1
									| x::xs -> 1 + unfold xs ) in (unfold b)
;;

let rec countall_ids a =
	match a with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> 0
									| x::[] -> all_st x
									| x::xs -> all_st x + unfold xs) in unfold l
and all_st x =							
	match x with
	| _ -> 1
;;

let rec createfsmDT fsm=	
	let ht = Hashtbl.create (countall_ids fsm) in
	match fsm with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> ht
									| x::[] -> createSt x ht ; ht
									| x::xs -> createSt x ht ; unfold xs ) in unfold l
and createSt st ht =
	match st with
	| InitialState (l,r) -> Hashtbl.add ht l (createValueState r ht st l)
	| State (l,r) -> Hashtbl.add ht l (createValueState r ht st l)
and createValueState tr ht st name=
	let ht2 = Hashtbl.create (count_trans st name) in
	match tr with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> ht2 
									| x::[] -> deal_tr x ht2 name ; ht2
									| x::xs -> deal_tr x ht2 name ; unfold xs ) in (unfold b)
and deal_tr actl ht name = (* TODO *)
	match actl with
	| ActionLine (l,m,r) -> let rec unfold l =
													(match l with
														| [] -> " " 
														| x::[] -> x
														| x::xs -> x^"" ) in Hashtbl.add ht l ([(unfold m); (nextstate r name)])
and nextstate t name =
	match t with
	| Trans l -> let rec unfold l =
											(match l with
											| [] ->  name
											| x::[] -> x
											| x::xs -> x^"") in (unfold l)
;;

let rec get_initial fsm =
	match fsm with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> ""
									| x::[] -> createSt x 
									| x::xs -> createSt x ^ unfold xs ) in unfold l
and createSt st =
	match st with
	| InitialState (l,_) -> l
	| _ -> ""
;;

let rec deal_input name ht queue inputs=
	if (Queue.length queue == 1) then
		(		
		let value = Hashtbl.find ht name in
			let x = Queue.peek queue in
				if List.mem x inputs then
    			(if (Hashtbl.mem value x)
  				then(
  				let b = Queue.pop queue in
  						let list = Hashtbl.find value b in
  							let temp1 = List.hd list in
  								let temp2 = List.nth list 1 in 
  									if temp1 = " " then "([],"^temp2^")\n"
  									else "(["^x^"],"^temp2^")\n")
  				else "ERROR1")
				else "ERROR2")
	else (
  	let value = Hashtbl.find ht name in
  			let x = Queue.peek queue in
					if (List.mem x inputs) then
    			(	if (Hashtbl.mem value x)
    				then(
      				let b = Queue.pop queue in
      						let list = Hashtbl.find value b in
      							let temp1 = List.hd list in
      								let temp2 = List.nth list 1 in 
    										let ans = deal_input temp2 ht queue inputs in
    											if (ans <> "ERROR1") then 
      										(	if temp1 = " " then "([],"^temp2^"),\n"^ans
      											else "(["^temp1^"],"^temp2^"),\n"^ans)
    											else "ERROR1" )
    				else "ERROR1"
					)
					else "ERROR2" )
;;

let rec read_input fsm is =
	let ht = createfsmDT fsm in
	let aux = Str.string_after is 1 in
		let length = String.length aux in
			let aux2 = Str.string_before aux (length-2) in 
				let queue = Queue.create () in
					let y a = Queue.push a queue in
						List.iter y (Str.split (Str.regexp ", ") aux2) ;
							let ini = get_initial fsm in
								let inputs = getInputs fsm [] in
									let ans = deal_input ini ht queue inputs in
										"[\n"^ans^"]."
;;