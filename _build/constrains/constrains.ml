open Syntax

exception Exec_err of string ;;


let count_broken temp1 temp2 temp3 temp4 temp5 =
	let y = ref 0 in
		if not temp1 then y := !y+1 else y:=!y ;
		if not temp2 then y := !y+1 else y:=!y ;
		if not temp3 then y := !y+1 else y:=!y ;
		if not temp4 then y := !y+1 else y:=!y ;
		if not temp5 then y := !y+1 else y:=!y ;
		string_of_int !y
;;

let rec all_ids a =
	match a with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> [all_st x]
									| x::xs -> (all_st x)::unfold xs) in unfold l
and all_st x =							
	match x with
	| InitialState (l,r) -> l
	| State (l,r) -> l
;;

let rec all_idsTrans a temp =
	match a with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> all_rst x temp
									| x::xs -> (all_rst x temp)@unfold xs) in unfold l
and all_rst x aux=
	match x with
	| InitialState (l,r) -> let y = l::aux in l::all_trs r y 
	| State (l,r) -> if List.mem l aux then all_trs r aux else []
and all_trs c y =
	match c with
	| AllTrans n -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> all_ac x y
									| x::xs -> (all_ac x y)@unfold xs) in (unfold n)
and all_ac d aux =
	match d with
	| ActionLine (l,m,r) -> all_t r aux
and all_t r aux =
	match r with
	| Trans s -> let unfold l =
											(match l with
											| x::[] -> if List.mem x aux then [] else x::aux ; [x]
											| _ -> [] ) in unfold s
;;

let rec union l1 l2 = 
    match l1 with
    | [] -> l2
    | h::t -> if List.mem h l2 then union t l2
              else union t (h::l2)
;;

let rec intersect l1 l2 =
	match l1 with 
		| [] -> []
		| h1::t1 -> if List.mem h1 l2 then h1::intersect t1 l2 else intersect t1 l2
;;

let print_bool a =
	if a then print_endline "True" else print_endline "False"
;;

let rec equalList l1 l2 =
	if List.length l1 = List.length l2 then 
		(let l3 = intersect l1 l2 in
			if List.length l1 = List.length l3 then true
			else false)
	else false
;;

let rec all_refIds fsm temp =
	match fsm with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> all_refst x temp
									| x::xs -> (all_refst x temp)@unfold xs) in unfold l
and all_refst x temp =
	match x with
	| InitialState (l,r) -> all_reftrs r temp
	| State (l,r) -> all_reftrs r temp
and all_reftrs c temp =
	match c with
	| AllTrans n -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> all_refac x temp
									| x::xs -> (all_refac x temp)@unfold xs) in (unfold n)
and all_refac d temp =
	match d with
	| ActionLine (l,m,r) -> all_reft r temp
and all_reft r temp =
	match r with
	| Trans s -> let unfold l =
											(match l with
											| x::[] -> if List.mem x temp then [] else x::temp ; [x]
											| _ -> [] ) in unfold s
;;

let rec isSubset l1 l2 =
	match l1 with
	| [] -> true
	| x::[] -> List.mem x l2
	| x::xs -> List.mem x l2 && isSubset xs l2
;;

let rec alltrans f =
	match f with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> [all_TPS x]
									| x::xs -> all_TPS x::unfold xs) in unfold l
and all_TPS x =							
	match x with
	| InitialState (l,r) -> count_trans r
	| State (l,r) -> count_trans r
and count_trans c =
	match c with
	| AllTrans n -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> [count_al x]
									| x::xs -> count_al x::unfold xs) in (unfold n)
and count_al d =
	match d with
	| ActionLine (l,m,r) -> l
;;

let rec countInit l =
	match l with
	| FSM l -> let rec unfold lt =
									(match lt with
									| [] -> 0
									| x::[] -> ct_st x
									| x::xs -> ct_st x + unfold xs) in unfold l
and ct_st x =							
	match x with
	| InitialState (l,r) -> 1
	| _ -> 0
;;

let singleInitial f =
	if countInit f == 1 then true
	else false
;;

let rec unique a list =
	match list with
	| [] -> 0
	| x::[] -> if x=a then 1 else 0
	| x::xs -> if x=a then 1 + (unique a xs) else 0 + (unique a xs)
;;	 

let distinctsID f = 
  	let rec unfold m =
  		(match m with
  		| [] -> true
  		| x::[] -> if (unique x m) = 1 then true else false
  		| x::xs -> if (unique x m) = 1 then true && unfold xs else false) in 
			let l = all_ids f in 
				unfold l
;;

let isDeterministic f =
	let rec unfold2 n = 
		(match n with
		| [] -> true
		| x::[] -> if (unique x n) = 1 then true else false
		| x::xs -> if (unique x n) = 1 then true && unfold2 xs else false) in 
	let rec unfold m =
  		(match m with
  		| [] -> true
  		| x::[] -> unfold2 x
  		| x::xs -> unfold2 x && unfold xs) in 
			let l = alltrans f in 
				unfold l
;;

let resolvable f =
	let ids = all_ids f in
		let refs = all_refIds f [] in
			isSubset refs ids
;;

let reachable f =
	let l1 = all_ids f in
		let t = all_idsTrans f [] in
			isSubset l1 t
;;

let check_contrains f =
	let temp5 = reachable f in
		let temp1 = singleInitial f in 
			let temp2 = distinctsID f in 
				let temp3 = isDeterministic f in
					let temp4 = resolvable f in
							let ans = ref "" in
								ans := (count_broken temp1 temp2 temp3 temp4 temp5)^" contrains broken:\n";
								if not temp5 then (ans := !ans^"* Can't reach all states trough initial state!\n");
		          	if not temp1 then (ans := !ans^"* There must be only one Initial State!\n") ;
		          	if not temp2 then (ans := !ans^"* All the ids should be unique!\n");
		          	if not temp3 then (ans := !ans^"* Nondeterministic!\n");
		          	if not temp4 then (ans := !ans^"* Unresolvable!");
							!ans
;;
