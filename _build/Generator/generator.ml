open Syntax
open Semantics

let rec getActions fsm temp=
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> getAcSt x temp
									| x::xs -> getAcSt x temp@ unfold xs) in unfold m
and getAcSt st temp =
	match st with
	| InitialState (_,r) -> getTrs r temp
  | State (_,r) -> getTrs r temp
and getTrs at temp =
	match at with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> get_tr x temp 
									| x::xs -> (get_tr x temp)@unfold xs ) in (unfold b)
and get_tr actl temp = 
	match actl with
	| ActionLine (_,m,_) -> let rec unfold l =
													(match l with
														| x::[] -> if List.mem x temp then [] else x::temp ; [x]
														| _ -> [] ) in unfold m
;;

let rec getStates fsm =
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> [getSt x]
									| x::xs -> getSt x ::unfold xs) in unfold m
and getSt st =
	match st with
	| InitialState (l,_) -> l
  | State (l,_) -> l
;;

let rec getInputs fsm temp=
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> getAcSti x temp
									| x::xs -> (getAcSti x temp)@ unfold xs) in unfold m
and getAcSti st temp=
	match st with
	| InitialState (_,r) -> getTrsi r temp
  | State (_,r) -> getTrsi r temp
and getTrsi at temp=
	match at with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> get_tri x temp 
									| x::xs -> (get_tri x temp)@unfold xs ) in (unfold b)
and get_tri actl temp = 
	match actl with
	| ActionLine (l,_,_) -> if List.mem l temp then [] else l::temp ; [l]
;;

let rec getAdds fsm =
	match fsm with
	| FSM m -> let rec unfold lt =
									(match lt with
									| [] -> []
									| x::[] -> getAdSt x
									| x::xs -> getAdSt x @ unfold xs) in unfold m
and getAdSt st =
	match st with
	| InitialState (l,r) -> getAdTrs r l
  | State (l,r) -> getAdTrs r l
and getAdTrs at id =
	match at with
	| AllTrans b -> let rec unfold l =
									(match l with
									| [] -> []
									| x::[] -> [getadd_tr x id]
									| x::xs -> (getadd_tr x id)::unfold xs ) in (unfold b)
and getadd_tr actl id = 
	match actl with
	| ActionLine (l,m,r) -> let rec unfold l =
													(match l with
														| [] -> " " 
														| x::[] -> x
														| x::xs -> x^"" ) in (id,l,(unfold m),(addTrans r id))
and addTrans r id =
	match r with
	| Trans s -> let unfold l =
								(match l with
									| [] -> id 
									| x::[] -> x
									| x::xs -> x ) in unfold s
;;
(* CREATES *)

let createAllAction fsm =
	let file = "../Java/Action.java" in 
		let oc = open_out file in
			let list = getActions fsm [] in
				let comment = "// Generated code\n" in
					let rec unfold l =
						(match l with
						| [] -> ""
						| x::[] -> x
						| x::xs -> x^", "^unfold xs) in
						let code = "public enum Action {"^unfold list^"}" in
							Printf.fprintf oc "%s\n" comment;
							Printf.fprintf oc "%s\n" code;
							close_out oc;	
;;


let createAllStates fsm =
	let file = "../Java/States.java" in 
		let oc = open_out file in
			let list = getStates fsm in
				let comment = "// Generated code\n" in
					let rec unfold l =
						(match l with
						| [] -> ""
						| x::[] -> x
						| x::xs -> x^", "^unfold xs) in
						let code = "public enum States {"^unfold list^"}" in
							Printf.fprintf oc "%s\n" comment;
							Printf.fprintf oc "%s\n" code;
							close_out oc;	
;;

let createAllInput fsm =
	let file = "../Java/Input.java" in 
		let oc = open_out file in
			let list = getInputs fsm [] in
				let comment = "// Generated code\n" in
					let rec unfold l =
						(match l with
						| [] -> ""
						| x::[] -> x
						| x::xs -> x^", "^unfold xs) in
						let code = "public enum Input {"^unfold list^"}" in
							Printf.fprintf oc "%s\n" comment;
							Printf.fprintf oc "%s\n" code;
							close_out oc;	
;;

let createFSM fsm =
	let file = "../Java/Fsm.java" in 
		let oc = open_out file in
			let list = getAdds fsm in
				let comment = "// Generated code\n" in
					let defineClass = "public class Fsm extends FSMBase<State, Input, Action> {\n" in
						let constructor = "\tpublic Fsm(HandlerBase<Action> handler) {\n" in
							let handler = "\t\tthis.handler = handler;\n" in
      					let rec unfold l =
      						(match l with
      						| [] -> ""
      						| (a,b,c,d)::[] -> if c = " " then 
																		("\t\tadd(State."^a^", Input."^b^", null, State."^d^");\n")
									 									else
																			("\t\tadd(State."^a^", Input."^b^", Action."^c^", State."^d^");\n")
      						| (a,b,c,d)::xs -> if c = " " then
																		("\t\tadd(State."^a^", Input."^b^", null, State."^d^");\n"^unfold xs)
									 									else
																			("\t\tadd(State."^a^", Input."^b^", Action."^c^", State."^d^");\n"^unfold xs)) in
      						let code = "\t\tstate = State."^(get_initial fsm)^";\n"^unfold list^"\n\t}\n}" in
      							Printf.fprintf oc "%s\n" comment;
      							Printf.fprintf oc "%s\n" defineClass;
										Printf.fprintf oc "%s\n" constructor;
										Printf.fprintf oc "%s\n" handler;
      							Printf.fprintf oc "%s\n" code;
      							close_out oc;	
;;

let rec create fsm =
	createAllAction fsm; 
	createAllStates fsm;
	createAllInput fsm;
	createFSM fsm;
;;

