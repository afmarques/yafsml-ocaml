type token =
  | EOF
  | NAME of (string)
  | INITIALSTATE
  | STATE
  | ARROW
  | BAR
  | SEMICOLON
  | RCBRA
  | LCBRA
  | DOT

val main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Syntax.fsm
