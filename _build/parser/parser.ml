type token =
  | EOF
  | NAME of (string)
  | INITIALSTATE
  | STATE
  | ARROW
  | BAR
  | SEMICOLON
  | RCBRA
  | LCBRA
  | DOT

open Parsing;;
# 2 "parser/parser.mly"
open Syntax
# 17 "parser/parser.ml"
let yytransl_const = [|
    0 (* EOF *);
  258 (* INITIALSTATE *);
  259 (* STATE *);
  260 (* ARROW *);
  261 (* BAR *);
  262 (* SEMICOLON *);
  263 (* RCBRA *);
  264 (* LCBRA *);
  265 (* DOT *);
    0|]

let yytransl_block = [|
  257 (* NAME *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\003\000\003\000\004\000\005\000\006\000\
\007\000\007\000\008\000\010\000\010\000\009\000\009\000\000\000"

let yylen = "\002\000\
\002\000\001\000\002\000\001\000\001\000\005\000\005\000\001\000\
\002\000\003\000\003\000\002\000\000\000\000\000\002\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\000\000\016\000\000\000\000\000\004\000\
\005\000\000\000\000\000\001\000\003\000\000\000\000\000\000\000\
\000\000\008\000\000\000\000\000\000\000\000\000\006\000\000\000\
\007\000\015\000\000\000\011\000\010\000\012\000"

let yydgoto = "\002\000\
\005\000\006\000\007\000\008\000\009\000\017\000\018\000\019\000\
\022\000\028\000"

let yysindex = "\004\000\
\001\255\000\000\000\255\005\255\000\000\254\254\001\255\000\000\
\000\000\002\255\003\255\000\000\000\000\007\255\007\255\004\255\
\006\255\000\000\008\255\009\255\011\255\013\255\000\000\007\255\
\000\000\000\000\014\255\000\000\000\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\010\255\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\252\254\
\000\000\000\000\000\000\000\000\000\000\012\255\000\000\015\255\
\000\000\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\013\000\000\000\000\000\000\000\006\000\255\255\000\000\
\000\000\000\000"

let yytablesize = 23
let yytable = "\014\000\
\010\000\014\000\003\000\004\000\001\000\011\000\012\000\016\000\
\021\000\014\000\015\000\026\000\023\000\024\000\030\000\025\000\
\027\000\013\000\002\000\013\000\020\000\009\000\029\000"

let yycheck = "\004\001\
\001\001\006\001\002\001\003\001\001\000\001\001\009\001\001\001\
\005\001\008\001\008\001\001\001\007\001\006\001\001\001\007\001\
\004\001\006\001\009\001\007\000\015\000\007\001\024\000"

let yynames_const = "\
  EOF\000\
  INITIALSTATE\000\
  STATE\000\
  ARROW\000\
  BAR\000\
  SEMICOLON\000\
  RCBRA\000\
  LCBRA\000\
  DOT\000\
  "

let yynames_block = "\
  NAME\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'states_list) in
    Obj.repr(
# 18 "parser/parser.mly"
                 (FSM(_1))
# 102 "parser/parser.ml"
               : Syntax.fsm))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'stateE) in
    Obj.repr(
# 22 "parser/parser.mly"
        ([_1])
# 109 "parser/parser.ml"
               : 'states_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'stateE) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'states_list) in
    Obj.repr(
# 23 "parser/parser.mly"
                     (_1::_2)
# 117 "parser/parser.ml"
               : 'states_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'initial_state) in
    Obj.repr(
# 27 "parser/parser.mly"
               (_1)
# 124 "parser/parser.ml"
               : 'stateE))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'state) in
    Obj.repr(
# 28 "parser/parser.mly"
         (_1)
# 131 "parser/parser.ml"
               : 'stateE))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'all_trans) in
    Obj.repr(
# 32 "parser/parser.mly"
                                         (InitialState(_2,_4))
# 139 "parser/parser.ml"
               : 'initial_state))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'all_trans) in
    Obj.repr(
# 36 "parser/parser.mly"
                                  (State(_2,_4))
# 147 "parser/parser.ml"
               : 'state))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'all_trans_list) in
    Obj.repr(
# 40 "parser/parser.mly"
                (AllTrans(_1))
# 154 "parser/parser.ml"
               : 'all_trans))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'trans_line) in
    Obj.repr(
# 44 "parser/parser.mly"
                      ([_1])
# 161 "parser/parser.ml"
               : 'all_trans_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'trans_line) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'all_trans_list) in
    Obj.repr(
# 45 "parser/parser.mly"
                                       (_1::_3)
# 169 "parser/parser.ml"
               : 'all_trans_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'action) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'transition) in
    Obj.repr(
# 49 "parser/parser.mly"
                        (ActionLine(_1,_2,_3))
# 178 "parser/parser.ml"
               : 'trans_line))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 53 "parser/parser.mly"
            (Trans([_2]))
# 185 "parser/parser.ml"
               : 'transition))
; (fun __caml_parser_env ->
    Obj.repr(
# 54 "parser/parser.mly"
   (Trans([]))
# 191 "parser/parser.ml"
               : 'transition))
; (fun __caml_parser_env ->
    Obj.repr(
# 58 "parser/parser.mly"
 ([])
# 197 "parser/parser.ml"
               : 'action))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 59 "parser/parser.mly"
            ([_2])
# 204 "parser/parser.ml"
               : 'action))
(* Entry main *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let main (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Syntax.fsm)
;;
