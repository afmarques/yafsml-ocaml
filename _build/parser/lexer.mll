{
open Parser
}

let char = ['a'-'z' 'A'-'Z']
let name = char*

rule token = parse
	| [' ' '\t' '\r' '\n'] { token lexbuf }
	| "initial state" { INITIALSTATE }
	| "state" { STATE }
	| '{' { LCBRA }
	| '}' { RCBRA } 
	| ';' { SEMICOLON }
	| '.' { DOT }
	| '/' { BAR } 
	| "->" {ARROW}
	| name as word { NAME (word) }
	| eof {EOF}