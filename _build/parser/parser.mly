%{
open Syntax
%}

%token EOF

%token <string> NAME
 
%token INITIALSTATE STATE ARROW BAR SEMICOLON RCBRA LCBRA DOT

%start main

%type <Syntax.fsm> main 

%% /* Grammar rules */

main:
	states_list DOT {FSM($1)}
;

states_list:
	stateE {[$1]}
	| stateE states_list{$1::$2}
;

stateE:
	initial_state {$1}
	| state	{$1}
;

initial_state:
	INITIALSTATE NAME LCBRA all_trans RCBRA {InitialState($2,$4)}
;

state:
	STATE NAME LCBRA all_trans RCBRA {State($2,$4)}
;

all_trans:
	all_trans_list {AllTrans($1)}
;

all_trans_list: 
	trans_line SEMICOLON {[$1]}
	| trans_line SEMICOLON all_trans_list {$1::$3}
;

trans_line:
	NAME action transition {ActionLine($1,$2,$3)}
;

transition:
	ARROW NAME {Trans([$2])}
	| {Trans([])}
;

action:
	{[]}
	| BAR NAME {[$2]}
;

%%